(function () {
  // thanks to http://stackoverflow.com/a/22885503
  var waitForFinalEvent=function(){var b={};return function(c,d,a){a||(a="I am a banana!");b[a]&&clearTimeout(b[a]);b[a]=setTimeout(c,d)}}();
  var fullDateString = new Date();
  function isBreakpoint(alias) {
    return $('.device-' + alias).is(':visible');
  }

  // GRID_ROWS and GRID_COLUMNS are globals defined in the template file

  var mainContainer = $("#main-container");
  var gridstackContainer = $(".grid-stack");

  gridstackContainer.gridstack({
      width: GRID_COLUMNS,
      cellHeight: 'auto'
  });

  function resizeGrid() {
    var grid = $('.grid-stack').data('gridstack');
    if (isBreakpoint('xs')) {
      $('#grid-size').text('One column mode');
    } else if (isBreakpoint('sm')) {
      grid.setGridWidth(3);
      $('#grid-size').text(3);
    } else if (isBreakpoint('md')) {
      grid.setGridWidth(6);
      $('#grid-size').text(6);
    } else if (isBreakpoint('lg')) {
      grid.setGridWidth(12);
      $('#grid-size').text(12);
    }
  };
  
  $(window).resize(function () {
    waitForFinalEvent(function() {
      resizeGrid();
    }, 300, fullDateString.getTime());
  });

})();